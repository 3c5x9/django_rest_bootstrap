from django.contrib.auth.models import User, Group
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework import viewsets
from api.models import RawMQTTData
from api.serializers import RawMQTTDataSerializer
from serializers import UserSerializer, GroupSerializer, RawMQTTDataSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

class RawMQTTDataViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows RawMQTTData to be viewed or edited.
    """
    queryset = RawMQTTData.objects.all()
    serializer_class = RawMQTTDataSerializer

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

#############
@csrf_exempt
def mqtt_list(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        snippets = RawMQTTData.objects.all()
        serializer = RawMQTTDataSerializer(snippets, many=True)
        return JSONResponse(serializer.data)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = RawMQTTDataSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)
    
@csrf_exempt
def mqtt_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        snippet = RawMQTTData.objects.get(pk=pk)
    except RawMQTTData.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = RawMQTTDataSerializer(snippet)
        return JSONResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = RawMQTTDataSerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data)
        return JSONResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        snippet.delete()
        return HttpResponse(status=204)
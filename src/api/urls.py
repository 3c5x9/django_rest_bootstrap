from django.conf.urls import url
from api import views

urlpatterns = [
    url(r'^mqtt/$', views.mqtt_list),
    url(r'^mqtt/(?P<pk>[0-9]+)/$', views.mqtt_detail),
]
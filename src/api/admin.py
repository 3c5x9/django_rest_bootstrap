from django.contrib import admin
from models import RawMQTTData
class RawMQTTDataAdmin(admin.ModelAdmin):
	list_display  = ['id','topic','created']
	list_filter   = ['topic', 'created']
	search_fields = ['topic', 'payload','created']
admin.site.register(RawMQTTData,RawMQTTDataAdmin)